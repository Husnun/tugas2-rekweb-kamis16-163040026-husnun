<?php echo validation_errors('<p style="color:red">', '</p>') ?>
    <?php 
      if($this->session->flashdata('alert')) {
        echo '<div class="alert alert-danger alert-message">';
        echo $this->session->flashdata('alert');
        echo "</div>";
      }
    ?>
<!-- class ini berfungsi untuk menampilkan form tambah aksesoris -->
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-files-o"></i> Tambah Aksesoris</h3>
    <ol class="breadcrumb">
      <!-- jika mengklik tulisan home, maka kembali ke halaman admin -->
      <li><i class="fa fa-home"></i><a href="<?php echo base_url('admin') ?>">Home</a></li>
      <li><i class="fa fa-files-o"></i>Tambah Aksesoris</li>
    </ol>
  </div>
</div>
<!-- Form validations -->
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url('admin/add') ?>" enctype="multipart/form-data">
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Merk : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="merk" type="text" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cemail" class="control-label col-lg-2">Jenis : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" type="text" name="jenis" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="curl" class="control-label col-lg-2">Bahan : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" type="text" name="bahan" required/>
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Harga : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="harga" type="text" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Stok : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="stok" type="text" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Gambar : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="gambar" type="file"/>
              </div>
            </div>
            <div class="form-group ">
              <label for="ccomment" class="control-label col-lg-2">Deskripsi : <span class="required">*</span></label>
              <div class="col-lg-10">
                <textarea class="form-control " id="ccomment" name="deskripsi" required></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
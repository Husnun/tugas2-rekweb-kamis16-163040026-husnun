<?php echo validation_errors('<p style="color:red">', '</p>') ?>
    <?php 
      if($this->session->flashdata('alert')) {
        echo '<div class="alert alert-danger alert-message">';
        echo $this->session->flashdata('alert');
        echo "</div>";
      }
    ?>
<!-- class ini berfungsi untuk mengubah data tapi blm jalan, hampir sama kaya tambah tampilannya, tp gatau kenapa ga jalan-->
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-files-o"></i> Edit Aksesoris</h3>
    <ol class="breadcrumb">
      <li><i class="fa fa-home"></i><a href="<?php echo base_url('admin') ?>">Home</a></li>
      <li><i class="fa fa-files-o"></i>Edit Aksesoris</li>
    </ol>
  </div>
</div>
<!-- Form validations -->
<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <div class="panel-body">
        <div class="form">
          <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url('admin/update_item') ?>" enctype="multipart/form-data">
            <?= form_open_multipart('admin/update_item/' . $data['id']); ?>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Merk : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="merk" type="text" value="<?= $data['merk'] ?>" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cemail" class="control-label col-lg-2">Jenis : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" type="text" name="jenis" value="<?= $data['jenis'] ?>" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="curl" class="control-label col-lg-2">Bahan :</label>
              <div class="col-lg-10">
                <input class="form-control" type="text" name="bahan" value="<?= $data['bahan'] ?>"/>
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Harga : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="harga" type="text" value="<?= $data['harga'] ?>" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Stok : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="stok" type="text" value="<?= $data['stok'] ?>" required />
              </div>
            </div>
            <div class="form-group ">
              <label for="cname" class="control-label col-lg-2">Gambar : <span class="required">*</span></label>
              <div class="col-lg-10">
                <input class="form-control" name="gambar" minlength="5" type="file" value="<?= $data['gambar'] ?>"required />
              </div>
            </div>
            <div class="form-group ">
              <label for="ccomment" class="control-label col-lg-2">Deskripsi :</label>
              <div class="col-lg-10">
                <textarea class="form-control " id="ccomment" name="deskripsi" value="<?= $data['deskripsi'] ?>" required></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-offset-2 col-lg-10">
                <button class="btn btn-primary" type="submit" name="submit">Save</button>
                <button class="btn btn-default" type="button"><a href="<?php echo base_url('admin/dashboard') ?>"></a>Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2>Kelola Aksesoris</h2>
        <div style="float:right">
            <a href="<?php echo base_url();?>admin/add" class="btn btn-primary">Tambah Aksesoris</a>
        </div>
        <div class="clearfix"></div>
        <!-- class ini berfungsi untuk menampilkan data yang ada didalam database -->
    </div>
    <div class="x_content">
        <table class="table table-striped table-bordered dt-responsive nowrap" id='datatable'>
            <thead>
                <tr>
                    <!-- untuk menentukan seberapa banyak kolom yang kita miliki -->
                    <th>#</th>
                    <th>Merk</th>
                    <th>Jenis</th>
                    <th>Bahan</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <!-- untuk mengeget data yang ada didalam database -->
                <?php
                $i = 1;
                foreach ($aksesoris -> result() as $items) : ?>
                    <!-- menggunakan foreach agar data tampil sesuai penambahan items -->
                <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $items->merk; ?></td>
                    <td><?php echo $items->jenis; ?></td>
                    <td><?php echo $items->bahan; ?></td>
                    <td><?php echo 'Rp ' . number_format($items->harga,0,',','.'); ?></td>
                    <td><?php echo $items->stok; ?></td>
                    <td>
                        <!-- untuk tombol yang ada didalam tabel -->
                        <a href="<?php echo base_url();?>admin/update_item" class="btn btn-warning"><i class="fa fa-refresh"> Edit</i></a>
                        <a href="<?php echo base_url('admin/delete/'.$items->id.'') ?>" class="btn btn-danger"><i class="fa fa-times"> Hapus</i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
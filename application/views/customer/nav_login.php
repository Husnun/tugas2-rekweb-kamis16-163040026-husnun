<div class="toggle-nav">
  <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
</div>
<!--logo start-->
<a href="<?php echo base_url('customer') ?>" class="logo">Aksesoris <span class="lite">Wanita</span></a>
<!--logo end-->
<div class="nav search-row" id="top_menu">
  <!--  search form start -->
  <ul class="nav top-menu">
    <li>
      <form action="<?php echo base_url('customer/search') ?>" method="get" class="navbar-form">
        <input class="form-control" placeholder="search" type="text" name="keywoard">
      </form>
    </li>
  </ul>
  <!--  search form end -->
</div>
<div class="top-nav notification-row">
  <!-- notificatoin dropdown start-->
  <ul class="nav pull-right top-menu">
    <!-- user login dropdown start-->
    <li class="dropdown" >
      <button action="<?php echo base_url();?>Login/index" class="btn btn-primary">Login</button>
      
    </li>
    <!-- user login dropdown end -->
  </ul>
  <!-- notificatoin dropdown end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
<div id="sidebar" class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu">
    <li class="active">
        <a href="?" class="nav-link">
          <i class="icon_house_alt"></i>
          <span >Aksesoris</span>
        </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <i class="icon_document_alt"></i>
        <span>Ikat Pinggang</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <i class="icon_document_alt"></i>
        <span>Dompet</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <i class="icon_document_alt"></i>
        <span>Jam Tangan</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <i class="icon_document_alt"></i>
        <span>Kacamata</span>
      </a>
    </li>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->
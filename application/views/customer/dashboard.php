<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">
	<title>Halaman Customer</title>
	<!-- Bootstrap CSS -->
  <link href="<?php echo base_url(); ?>admin_asset/css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="<?php echo base_url(); ?>admin_asset/css/bootstrap-theme.css" rel="stylesheet">
  <!-- Data tables -->
    <link href="<?php echo base_url(); ?>admin_asset/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>admin_asset/css/responsive.bootstrap.min.css" rel="stylesheet">
    <!-- font icon -->
  <link href="<?php echo base_url(); ?>admin_asset/css/elegant-icons-style.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>admin_asset/css/font-awesome.min.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="<?php echo base_url(); ?>admin_asset/css/widgets.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>admin_asset/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>admin_asset/css/style-responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>admin_asset/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
</head>
<body>
 <!-- container section start -->
  <section id="container" class="">
    <header class="header dark-bg">
<?php $this->load->view('customer/nav_login'); ?>
<!--main content start-->
    <section id="main-content">
      <section class="wrapper">
          <?= $contents; ?>
      </section>
      <div class="text-right">
        <div class="credits">
          Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
      </div>
    </section>
    <!--main content end-->
  </section>

<!-- javascripts -->
  <script src="<?php echo base_url(); ?>admin_asset/js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>admin_asset/js/jquery-ui-1.10.4.min.js"></script>
  <script src="<?php echo base_url(); ?>admin_asset/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>admin_asset/js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- Data Tables -->
    <script src="<?php echo base_url(); ?>admin_asset/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_asset/js/dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>admin_asset/js/responsive.min.js"></script>
    <!-- bootstrap -->
  <script src="<?php echo base_url(); ?>admin_asset/js/bootstrap.min.js"></script>
  <!--custome script for all page-->
    <script src="<?php echo base_url(); ?>admin_asset/js/scripts.js"></script>
    <script>
      $(document).ready(function(){
        $('#datatable').DataTable()
      })
      $('.alert-message').alert().delay(3000).slideUp('slow');
    </script>
</body>
</html>



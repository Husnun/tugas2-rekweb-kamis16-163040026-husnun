<div class="x_panel">
    <div class="x_title">
        <h2>Aksesoris</h2>
        
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
       <?php foreach($aksesoris -> result() as $items) : ?>
            <div class="col-sm-4"> 
              <div class="card" style="width: 300px; height: 450px;border-style: outset; ">
                <div class="card-body">
                  <img src="<?= base_url('./asset/upload/') .$items->gambar ?>" style="width:60% " class="card-img-top" alt="card-image">
                  <center>
                    <h4 class="card-title"><?php echo $items->merk; ?></h4>
                    <p class="card-text"><?php echo $items->jenis; ?></p>
                    <h4 class="card-title"><?php echo 'Rp ' . number_format($items->harga,0,',','.'); ?></p></h4>
                    <p class="card-text">Stok :<?php echo $items->stok; ?>
                    <br><br>
                    <a href="<?php echo site_url('customer/detail/')?>"" class="btn btn-success"><i class="fa fa-check"> Detail</i></a>
                    <a href="<?php echo base_url();?>customer/add" class="btn btn-primary">Pesan Sekarang</a>
                  </center>
                </div>
              </div>
              <br>
            </div> 
          <?php endforeach; ?>

    </div>
   
    </div>
</div>

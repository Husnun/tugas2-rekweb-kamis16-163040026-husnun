
<div class="col-sm-4">  
              <div class="card" style="width: 250px; height: 450px;border-style: outset; ">
                <div class="card-body">
                  <img src="<?= base_url('asset/upload/Aksesoris/') .$items->gambar ?>" style="width:100%" class="card-img-top" alt="card-image">
                  <center>
                    <h4 class="card-title"><?php echo $items->merk; ?></h4>
                    <p class="card-text"><?php echo $items->jenis; ?></p>
                    <p class="card-text"><?php echo $items->bahan; ?></p>
                    <h4 class="card-title"><?php echo 'Rp ' . number_format($items->harga,0,',','.'); ?></p></h4>
                    <p class="card-text">Stok :<?php echo $items->stok; ?>
                    <p class="card-text"><?php echo $items->deskripsi; ?></p>
                    <br><br>
                    <a href="<?php echo base_url();?>admin/detail" class="btn btn-success"><i class="fa fa-check"> Detail</i></a>
                    <a href="<?php echo base_url();?>admin/add" class="btn btn-primary">Pesan Sekarang</a>
                  </center>
                  <br>
                </div>
              </div>
              <br>
            </div> 
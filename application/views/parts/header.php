<!DOCTYPE html>
<html>
	<head>
		<title>My To Do List</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
		<!-- styles -->
		<link href="<?php echo base_url('assets/css/styles.css') ?>" rel="stylesheet">
	</head>
	<body>
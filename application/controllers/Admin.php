<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    //untuk meload data library ci
    function __construct() {
        parent::__construct();
        $this->load->model('aksesoris_model');
        $this->load->library('form_validation');
    }
    //untuk menampilkan data ke dalam web
    public function index() {
        $aksesoris['aksesoris'] = $this->aksesoris_model->get_all('aksesoris');
        $this->template->admin('admin/dashboard', 'admin/manage_data', $aksesoris);
    }
    //untuk menghapus data
    public function delete($id) {
        $this->aksesoris_model->delete($id);
        $this->session->set_flashdata('alert', 'Data berhasil dihapus!');
        redirect('admin');
    }
    //untuk menambah data, dilakukan validasi terlebih dahulu
    public function add() {
        
        $this->form_validation->set_rules('merk', 'Merk', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'numeric');
        $this->form_validation->set_rules('stok', 'Stok', 'numeric');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        // jika validasi gagal maka tampilkan data_form
        if($this->form_validation->run() == false) {
            $this->template->admin('admin/dashboard', 'admin/data_form');
        } else { //jika validasi berhasil maka configurasi gambar
            $config['upload_path'] = './asset/upload/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = "gambar" . time();

            $this->load->library('upload', $config);
            // lalu upload gambar yg diinputkan jika gambar sesuai
            $this->upload->do_upload('gambar');
                $gbr = $this->upload->data(); // data gambar dimasukan ke dalam variable $gbr
                // data yang diinputkan beserta gambar akan di post ke dalam database
            $data = [
                "merk" => $this->input->post('merk',true),
                "jenis" => $this->input->post('jenis',true),
                "bahan" => $this->input->post('bahan',true),
                "harga" => $this->input->post('harga',true),
                "stok" => $this->input->post('stok',true),
                "deskripsi" => $this->input->post('deskripsi',true),
                "gambar" => $gbr['file_name']// gambar yg diupload namanya diubah menjadi "gambar".time()
            ];
            //setelah menginputkan data, maka akan pindah ke function didalam model yang bernama tambahDataProduk($data)
            $this->aksesoris_model->tambahDataProduk($data);
            //data berhasil ditambahkan
            $this->session->set_flashdata('flash', 'Data berhasil ditambahkan');
            redirect('admin/add');
        }
    }
    //update item blm beres btw    
    public function update_item($id) {
        $this->form_validation->set_rules('merk', 'Merk', 'required');
        $this->form_validation->set_rules('bahan', 'Bahan', 'required');
        $this->form_validation->set_rules('jenis', 'Jenis', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'numeric');
        $this->form_validation->set_rules('stok', 'Stok', 'numeric');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        
        if($this->form_validation->run() == false) {
            $this->template->admin('admin/dashboard', 'admin/edit_form');
        } else { 
            $config['upload_path'] = './asset/upload/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['file_name'] = "gambar" . time();

            $this->load->library('upload', $config);
            
            $this->upload->do_upload('gambar');
                $gbr = $this->upload->data(); 

            $data = [
                "merk" => $this->input->post('merk',true),
                "jenis" => $this->input->post('jenis',true),
                "bahan" => $this->input->post('bahan',true),
                "harga" => $this->input->post('harga',true),
                "stok" => $this->input->post('stok',true),
                "deskripsi" => $this->input->post('deskripsi',true),
                "gambar" => $gbr['file_name']
            ];
            $status = $this->aksesoris_model->update($data, $id);

                if ($status) {
                    $this->session->set_flashdata('status', 'aksesoris berhasil diubah');
                } else {
                    $this->session->set_flashdata('status_salah', 'aksesoris gagal diubah');
                }
            $this->aksesoris_model->update($data, $data);
            
            $this->session->set_flashdata('flash', 'Data berhasil ditambahkan');
            redirect('admin/update_item');
        }
    }
    // cari juga blm jalan :((
    public function cari(){
        $this->load->model('aksesoris_model');
        $keyword = $this->input->get('keyword');
        $data['aksesoris'] = $this->aksesoris_model->cari($keyword)->result();
        $data['contents'] = 'admin/manage_data';

        $this->load->view('admin/dashboard', $data);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    //untuk meload data library ci
    function __construct() {
        parent::__construct();
        $this->load->model('aksesoris_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $aksesoris['aksesoris'] = $this->aksesoris_model->get_all('aksesoris');
        $this->template->customer('customer/dashboard', 'customer/manage_data', $aksesoris);
    }
    public function detail(){
        $data = array(
            'judul' => "Daftar Aksesoris",
            'aksesoris' => $this->aksesoris_model->get_all($table)
        );
        
        $this->view('customer/detail', $data);
        $this->template->customer('customer/dashboard', 'customer/detail', $aksesoris);
    }

    public function search(){
            $keyword = $this->input->post('keyword');
            $data['aksesoris']=$this->Aksesoris_model->get_product_keyword($keyword);
            $this->load->view('search',$data);
            $this->template->customer('customer/dashboard', 'customer/manage_data');
    }

}

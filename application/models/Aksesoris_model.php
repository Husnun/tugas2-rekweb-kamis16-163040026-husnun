<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

//memasukan data didalam database
    public function tambahDataProduk($data) {
        $this->db->insert('aksesoris', $data);
        return true;
    }
    
//mengambil semua data di database
    function get_all($table) {
        $this->db->from($table);
        return $this->db->get();
    }
    //menghapus data didalam database berdasarkan id
    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('aksesoris');
        return $this->db->affected_rows();
    }
    
    //update data dr database, btw ini blm jalan jg
    function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update('aksesoris', $data);
        return $this->db->affected_rows();
    }
    // cari data berdasarkan keyword atau inputan yg dimasukan, btw ini jg blm jalan
    public function cari($keyword) {
        $this->db->select('*');
        $this->db->from('aksesoris');
        $this->db->like('merk', $keyword);
        return $this->db->get();
    }
}
 ?>
 
